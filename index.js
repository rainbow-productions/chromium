// index.js
import express from 'express';
import run from './api/run.js';

const app = express();

app.use('/api/run', run);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
