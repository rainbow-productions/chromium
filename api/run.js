// api/run.js
import { join } from 'path';
import express from 'express';
import edgeChromium from 'chrome-aws-lambda';
import puppeteer from 'puppeteer-core';

const LOCAL_CHROME_EXECUTABLE = '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome';

const app = express();

app.get('/', async (req, res) => {
  // Edge executable will return an empty string locally.
  const executablePath = await edgeChromium.executablePath || LOCAL_CHROME_EXECUTABLE;

  const browser = await puppeteer.launch({
    executablePath,
    args: edgeChromium.args,
    headless: true, // Set to true for Vercel deployment
  });

  const page = await browser.newPage();
  const htmlPath = join(__dirname, 'index.html'); // Correct path to your HTML template
  await page.goto(`file://${htmlPath}`);

  // Capture a screenshot of the rendered page
  const screenshot = await page.screenshot({ encoding: 'base64' });

  // Send the screenshot as a response
  res.send(`<img src="data:image/png;base64,${screenshot}" alt="Chromium Screenshot">`);

  await browser.close();
});

export default app;
